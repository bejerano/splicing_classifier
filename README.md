#S-CAP - Splicing Clinically Applicable Pathogenicity score

## Overview
S-CAP is the first pathogenicity classifier designed to predict the pathogenicity of splice affecting rare single nucleotide variants in patient genomes with the high sensitivity required in the clinic. S-CAP combines sequenced-based features, evolutionary conservation, and existing metrics including SPIDEX, CADD, and LINSIGHT. S-CAP is comprised of 6 separate models, each designed to predict the pathogenicity of rare single nucleotide variants in a different splicing region, as is described in our paper. Further details can be found in our paper.

## S-CAP data processing

### Step 1. Variant Annotation 

Here is the script used to annotate a VCF with all the feature information that we use

```
./src/annotate_features/annotate_features.sh <in_vcf> <base_dir> <variant_type>
```

### Step 2. Variant Filtering
We filter out variants that are not in the splicing region (as defined in the text), not synonymous or common in the control population (ExAC and gnomAD).

```
./src/annotate_features/split_filter_alleles.py <in_vcf.features> <in_vcf.features.filter> <variant_type>
```

### Step 3. Transform data in a feature matrix
The VCF which is annotating each variant with feature information is parse and turned into a feature matrix which we can use for downstream processing

```
./src/annotate_features/numpify.py <in_vcf.features.filter> <in_vcf.features.filter.tsv>
```

## Feature categories

* Sequence Features
* Evolutionary Conservation Features
* Existing metrics -  SPIDEX, LINSIGHT, CADD, etc

## Training

We train the S-CAP model using a gradient boosting tree model incorporating over 100 features to measure splicing pathgenocity

## Data

S-CAP scores are precomputed for all possible rare single nucleotide variants in the splicing region.

The scores can be found [here](http://bejerano.stanford.edu/scap/downloads/dat/).

