from scipy import interp
from sklearn.utils import shuffle
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import roc_curve, auc, roc_auc_score, precision_recall_curve
from sklearn.metrics.ranking import _binary_clf_curve
from collections import defaultdict
from random import randrange, seed

import copy
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.style.use('classic')

import numpy as np

from scap import Scap
from scapOld import ScapOld

seed_number=715926538


colors = {
    "S-CAP":    'b',
    "SPIDEX":   'g',
    "CADD":     'm',
    "LINSIGHT": 'r',
    "EIGEN":    'c',
    "TRAP":     'y',
    "CDTS":     'k',
}

markers = {
    "S-CAP":    '',
    "SPIDEX":   '',
    "CADD":     '',
    "LINSIGHT": '',
    "EIGEN":    's',
    "TRAP":     'o',
    "CDTS":     '^',
}

linestyles = {
    "S-CAP":    '-',
    "SPIDEX":   '-.',
    "CADD":     '--',
    "LINSIGHT": ':',
    "EIGEN":    '-',
    "TRAP":     '-',
    "CDTS":     '-',
}

def count(chroms):
    """
    Returns a dictionary
    {CHROM1: COUNT1,
     CHROM2: COUNT2,
     ...}
    """
    c = {}
    for chrom in map(str, chroms):
        if chrom not in c: c[chrom] = 0
        c[chrom] += 1
    return c

def counts_tuples(v1, v2):
    """
    Given v1, v2 np arrays with chromosome IDs
    in the oth column.
    
    Return a list of tuples:
    [(CHROM, v1 count, v2 count), ...].
    """
    v1_counts, v2_counts = count(v1), count(v2)
    counts = []
    for chrom in set(v1_counts.keys()).union(set(v2_counts.keys())):
        c1 = v1_counts[chrom] if chrom in v1_counts else 0
        c2 = v2_counts[chrom] if chrom in v2_counts else 0
        counts += [(chrom, c1, c2)]
    return counts

def partition(counts, folds):
    """
    Assign each chromosome in COUNTS to one of
    FOLDS groups.
    
    Attempts to make the first variant counts,
    as even as possible, but does so only approximately.
    
    This is an instance of the Partition Problem:
    https://en.wikipedia.org/wiki/Partition_problem
    generalized to an arbitraty number of partitions.
    """
    groups = {}
    total1 = [0 for _ in range(folds)]
    total2 = [0 for _ in range(folds)]
    seed(seed_number)
    for chrom, c1, c2 in sorted(counts, key = lambda x: -x[1]):
        if c1:
            i = total1.index(min(total1))
        else:
            i = randrange(folds)
        groups[chrom] = i
        total1[i] += c1
        total2[i] += c2
    return groups

def group(variants, groups):
    """
    The first column of variants must be a chromosome
    ID present in groups. This column is not present in
    the output.
    
    Returns a list, X, of np arrays
    where rows(X[i]) = all variants on a chromsome
    in group i.
    
    All np arrays are shuffled prior to return.
    """
    X = [[] for _ in range(len(set(groups.values())))]
    for var in variants:
        X[groups[str(var[0])]] += [var[1:]]
    return map(lambda x: shuffle(np.array(x), random_state=seed_number), X)

def distribute(feat1, chrom1, feat2, chrom2, chroms):
    assert feat1.shape[0] == chrom1.shape[0]
    assert feat2.shape[0] == chrom2.shape[0]

    # Actually add variants to groups and shuffle them
    X1 = group(np.hstack([chrom1.values.reshape(-1, 1), feat1]), chroms)
    X2 = group(np.hstack([chrom2.values.reshape(-1, 1), feat2]), chroms)
      
    # Downsample
    X2 = map(lambda x: x[:min(map(len, X2))], X2)
    
    # Create y vector
    Y1 = [np.ones((len(x),))  for x in X1]
    Y2 = [np.zeros((len(x),)) for x in X2]
    
    # Shuffle positives with negatives
    Y = [shuffle(np.hstack([y1, y2]), random_state=seed_number) for y1, y2 in zip(Y1, Y2)]
    
    X = [shuffle(np.vstack([x1, x2]), random_state=seed_number) for x1, x2 in zip(X1, X2)]
    
    for x, y in zip(X,Y):
        assert x.shape[0] == y.shape[0], (x.shape, y.shape)
    return X, Y

###############################################################

def calculate_tnr_fnr(X, Y):
    fps, tps, thresholds = _binary_clf_curve(Y, X)
    fns = tps[-1] - tps
    tns = fps[-1] - fps

    tnr = tns / (tns + fps + 1e-12)
    tnr = tnr[::-1]

    fnr = fns / (tps + fns + 1e-12)
    fnr = fnr[::-1]

    return [tnr, fnr]

def train_final_model(X_train, X_test, Y_train, Y_test, model, features, test_sets, comp_sets):
    X_train = np.vstack(X_train)
    Y_train = np.hstack(Y_train)
        
    """
    print(X_train.shape, Y_train.shape, X_test.shape, Y_test.shape)
    print("training pathogenic: %d, benign: %d"%(np.sum(Y_train==1), np.sum(Y_train==0)))
    print("testing pathogenic: %d, benign: %d"%(np.sum(Y_test==1), np.sum(Y_test==0)))
    """
    
    m = model.fit(X_train, Y_train)
    probs = m.predict_proba(X_test)
    fpr, tpr, thresholds = roc_curve(Y_test, probs[:,1])
    p, r, t = precision_recall_curve(Y_test, probs[:,1])  
    recall = np.linspace(0, 1, 20)
    r, p = zip(*sorted(zip(r, p)))
    precision = interp(recall, r, p)
    tnr, fnr = calculate_tnr_fnr(probs[:,1], Y_test)
    
    """
    print ("Testing the final model")
    print ("Test set auc: {}".format(roc_auc_score(Y_test, probs[:,1])))
    """ 
    return copy.copy(m) 
    
def train_a_model(X, Y, X_eval, Y_eval, idx, model):
    X_train = np.vstack([x for i, x in enumerate(X) if i != idx])
    Y_train = np.hstack([x for i, x in enumerate(Y) if i != idx])

    m = model.fit(X_train, Y_train)
    probs = m.predict_proba(X_eval[idx])

    return Y_eval[idx], probs[:,1], m

def cross_validation(X, Y, X_eval, Y_eval, model, title, features):
    Y_labels, Y_predicts, folds_aucs, folds_praucs = [], [], [], []
    for idx in range(len(X)):
        Y_label, Y_predict, m = train_a_model(X, Y, X_eval, Y_eval, idx, model)
        print Y_label.shape, Y_predict.shape
        Y_labels.append(Y_label)
        Y_predicts.append(Y_predict)
        folds_aucs.append(roc_auc_score(Y_label, Y_predict))
        p, r, t = precision_recall_curve(Y_label, Y_predict)
        folds_praucs.append(auc(r, p))
        
    # join all the test data and their predicted scores to show an AUC curve 
    Y_labels = np.hstack(Y_labels)
    Y_predicts = np.hstack(Y_predicts)
    
    print Y_labels.shape, Y_predicts.shape
    print Y_labels, Y_predicts
    
    fpr, tpr, thresholds = roc_curve(Y_labels, Y_predicts)
    p, r, t = precision_recall_curve(Y_labels, Y_predicts)  
    recall = np.linspace(0, 1, 20)
    r, p = zip(*sorted(zip(r, p)))
    precision = interp(recall, r, p)
    tnr, fnr = calculate_tnr_fnr(Y_predicts, Y_labels)
    
    #print sorted(zip(Y_predicts, Y_labels), reverse=True)[:10]
    
    
    a = auc(fpr, tpr)
    fig, ax = plt.subplots()
    ax.plot(fpr, tpr, label = "SCAP: auc=%.2f"%(a))
    ax.set_xlabel("FPR")
    ax.set_ylabel("TPR")
    ax.set_title(title)
    ax.legend(loc=4, fontsize=12)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.patch.set_facecolor('white')
    plt.savefig(title + ".pdf")
    plt.show()
    pr_a = auc(recall, precision)
    print(pr_a)
    a95 = 20*(a-auc(fpr, map(lambda x: min([x, .95]), tpr)))
    
    print ("ROC-AUC and PR-AUC over all CV folds")
    print ("gene_cv auc: {}".format(a))
    print ("gene_cv pr_auc: {} with {} pathogenic and {} benign".format(pr_a, sum(Y_labels==1), sum(Y_labels==0)))
    return a, pr_a, np.std(folds_aucs), np.std(folds_praucs)

def grid_search(X, X_eval, X_test, Y, Y_eval, Y_test, model, features, test_sets, comp_sets, region):
    # try a grid search of random paramters
    """
    param_performance = {}
    for i in range(1):
        lr = np.exp(np.random.uniform(np.log(0.0001),np.log(0.1)))
        n_estimators=int(np.random.uniform(700,1000))
        #max_depth = int(np.random.uniform(3,10))
        if i == 0:
            model = GradientBoostingClassifier()
        else:
            model = GradientBoostingClassifier(learning_rate=lr, n_estimators=n_estimators)
        a, pr_a, std_auc, std_prauc = cross_validation(X,Y,X_eval,Y_eval,model,title,features)
        param_performance["%d_%d"%(lr, n_estimators)] = (a, std_auc, pr_a, std_prauc, model)
    
    #train a model of all the data
    gridsearch_results = param_performance.values()
    sorted(gridsearch_results, reverse=True)
     
    a, std_auc, pr_a, std_prauc, model = gridsearch_results[0]
    """
    #cross_validation(X, Y, X_eval, Y_eval, model, "%s Average ROC Curve"%region, features)
    m = train_final_model(X, X_test, Y, Y_test, model, features, test_sets, comp_sets)    
    return m

#############################################################################
def exonic(v):
    return (v["5'Dist"] < -3) & (v["3'Dist"] >  0) & (v['Exon_pos'] != 2) & (v['total']+1 != v['Exon_pos'])

def extended(v):
    return (v["5'Dist"] >= 2) & (v["5'Dist"] < 6)

def inner_exonic(v):
    return (v["5'Dist"] >= -3) & (v["5'Dist"] < 0)

def supporting(v):
    return extended(v) | inner_exonic(v)

def get_5core(data):
    return data[(data["5'Dist"] >= 0) & (data["5'Dist"] < 2)]

def get_3core(data):
    return data[(data["3'Dist"] > -2) & (data["3'Dist"] <= 0)]

def get_exonic(data):
    return data[exonic(data)]

def get_5supporting(data):
    return data[supporting(data)]

def get_5intronic(data):
    return data[(data["5'Dist"] >= 6) & (data["5'Dist"] < 50)]

def get_3intronic(data):
    return data[(data["3'Dist"] > -50) & (data["3'Dist"] <= -2)]

def list_featurize(datasets, regions, features):
    return [featurize(dataset, features[region]) for region, dataset in zip(regions, datasets)]

def list_comparize(datasets, comparisons):
    return [comparize(dataset, comparisons) for dataset in datasets]

def split_train_byregion(d, datatype="benign"):
    d3i,d3c,dex,d5c,d5s,d5i = get_3intronic(d),get_3core(d),get_exonic(d),get_5core(d),get_5supporting(d),get_5intronic(d)

    
    alldata = pd.concat([d3i, d3c, dex, d5c, d5s, d5i])    
    d5i = pd.concat([d5s, d5i])
    dex = alldata
    
    if datatype=="pathogenic":
        d3c_dom = d[(d["3'Dist"] > -2) & (d["3'Dist"] <= 0) & (d['DOM'] == 1)]
        d3c_rec = d[(d["3'Dist"] > -2) & (d["3'Dist"] <= 0) & (d['DOM'] == 0)]        
        d5c_dom = d[(d["5'Dist"] >= 0) & (d["5'Dist"] < 2) & (d['DOM'] == 1)]
        d5c_rec = d[(d["5'Dist"] >= 0) & (d["5'Dist"] < 2) & (d['DOM'] == 0)]
    else:
        d3c_dom = d[(d["3'Dist"] > -2) & (d["3'Dist"] <= 0) & (d['DOM'] == 1)]
        d3c_rec = d[(d["3'Dist"] > -2) & (d["3'Dist"] <= 0) & (d['DOM'] == 0)]
        d5c_dom = d[(d["5'Dist"] >= 0) & (d["5'Dist"] < 2) & (d['DOM'] == 1)]        
        d5c_rec = d[(d["5'Dist"] >= 0) & (d["5'Dist"] < 2) & (d['DOM'] == 0)]
    
    regions = ["All", "3' Intronic", "3' Core", "3' Core Dom", "3' Core Rec", 
               "Exonic", "5' Core", "5' Core Dom", "5' Core Rec", "5' Extended", "5' Intronic"]
    return [alldata,d3i,d3c,d3c_dom,d3c_rec,dex,d5c,d5c_dom,d5c_rec,d5s,d5i], regions

def split_test_byregion(d, datatype="benign"):
    d3i,d3c,dex,d5c,d5s,d5i = get_3intronic(d),get_3core(d),get_exonic(d),get_5core(d),get_5supporting(d),get_5intronic(d)
    alldata = pd.concat([d3i, d3c, dex, d5c, d5s, d5i])
    
    if datatype=="pathogenic":
        d3c_dom = d[(d["3'Dist"] > -2) & (d["3'Dist"] <= 0) & (d['DOM'] == 1)]
        d3c_rec = d[(d["3'Dist"] > -2) & (d["3'Dist"] <= 0) & (d['DOM'] == 0)]        
        d5c_dom = d[(d["5'Dist"] >= 0) & (d["5'Dist"] < 2) & (d['DOM'] == 1)]
        d5c_rec = d[(d["5'Dist"] >= 0) & (d["5'Dist"] < 2) & (d['DOM'] == 0)]
    else:
        d3c_dom = d[(d["3'Dist"] > -2) & (d["3'Dist"] <= 0) & (d['DOM'] == 1)]
        d3c_rec = d[(d["3'Dist"] > -2) & (d["3'Dist"] <= 0) & (d['DOM'] == 0)]
        d5c_dom = d[(d["5'Dist"] >= 0) & (d["5'Dist"] < 2) & (d['DOM'] == 1)]        
        d5c_rec = d[(d["5'Dist"] >= 0) & (d["5'Dist"] < 2) & (d['DOM'] == 0)]
    
    regions = ["All", "3' Intronic", "3' Core", "3' Core Dom", "3' Core Rec", 
               "Exonic", "5' Core", "5' Core Dom", "5' Core Rec", "5' Extended", "5' Intronic"]
    return [alldata,d3i,d3c,d3c_dom,d3c_rec,dex,d5c,d5c_dom,d5c_rec,d5s,d5i], regions

def get_traintestsets_from_data(p, b, p_eval, b_eval, features, comparisons):
    # split data up by region
    p_datasets, regions = split_train_byregion(p, datatype="pathogenic")
    b_datasets, regions = split_train_byregion(b)
    
    p_eval_datasets, regions = split_test_byregion(p_eval, datatype="pathogenic")
    b_eval_datasets, regions = split_test_byregion(b_eval)
    
    chroms = partition(counts_tuples(p['gene_common_name'],
                                     np.hstack([p_eval['gene_common_name'],
                                                b['gene_common_name'],
                                                b_eval['gene_common_name']])), 6)

        
    p_datasets_feats = list_featurize(p_datasets, regions, features)
    b_datasets_feats = list_featurize(b_datasets, regions, features)
    
    p_eval_datasets_feats = list_featurize(p_eval_datasets, regions, features)
    b_eval_datasets_feats = list_featurize(b_eval_datasets, regions, features)

    p_datasets_comps = list_comparize(p_eval_datasets, comparisons)
    b_datasets_comps = list_comparize(b_eval_datasets, comparisons)

    train_sets, test_sets, comp_sets = [], [], []
    for p_data, p_eval_data, \
    p_feats, p_eval_feats, b_data, \
    b_eval_data, b_feats, b_eval_feats, \
    p_comps, b_comps, region in zip(p_datasets, p_eval_datasets, 
                                    p_datasets_feats, p_eval_datasets_feats, 
                                    b_datasets, b_eval_datasets,
                                    b_datasets_feats, b_eval_datasets_feats,
                                    p_datasets_comps, b_datasets_comps, regions):
        
        X_train, Y_train = distribute(p_feats, p_data['gene_common_name'],
                          b_feats, b_data['gene_common_name'], chroms)
        
        X_test, Y_test = distribute(p_eval_feats, p_eval_data['gene_common_name'],
                                    b_eval_feats, b_eval_data['gene_common_name'], chroms)

        C, _Y = distribute(p_comps, p_eval_data['gene_common_name'], 
                          b_comps, b_eval_data['gene_common_name'], chroms)

        test_sets.append((X_test[-1], Y_test[-1]))
        train_sets.append((X_train[:-1], Y_train[:-1]))
        comp_sets.append( C[-1])
    
    return dict(zip(regions, train_sets)), dict(zip(regions, test_sets)), dict(zip(regions, comp_sets)), chroms

def plot_precisionrecall_curve(scap, regions):
    COMPS = ["SPIDEX", "CADD", "LINSIGHT", "EIGEN", "EIGEN_NONCODING", "TRAP", "CDTS"]
    fig, ax = plt.subplots(2, len(regions), figsize=(len(regions)*7, 11), facecolor="white")
    for i, region in enumerate(regions):
        test_X, test_Y = scap.test_sets[region]
        comps = scap.comp_sets[region]
        for idx, metric in [(None, 'S-CAP')]+list(enumerate(COMPS)):
            if metric == "S-CAP":
                scores = scap.predict(region, test_X)[:,1]
            elif metric == "SPIDEX" or metric == "CDTS":
                scores = -comps[:,idx]
            else:
                scores = comps[:,idx]
        
            #### Throws out inf
            valid = (scores != float('inf')) * (scores != -float('inf'))
            #print region, metric, sum(valid), len(scores)
            if sum(valid) < len(scores)/2:
                continue
                
            if metric == 'EIGEN_NONCODING': metric = 'EIGEN'
            
            Y = test_Y[valid]
            X = scores[valid]
            
            p, r, t = precision_recall_curve(Y, X)  
            recall = np.linspace(0, 1, 20)
            r, p = zip(*sorted(zip(r, p)))
            precision = interp(recall, r, p)

            a = auc(recall, precision)
            a95 = 20*(a - auc(recall, map(lambda x: min([x,.95]), precision)))
            
            ax[0, i].plot(recall, precision, c = colors[metric],
                          label = "{0}:{1} auc={2:.3f}".format(metric, ' '*(8-len(metric)), a))
            ax[1, i].plot(recall, precision, c = colors[metric],
                          label = "{0}:{1} auc={2:.3f}".format(metric, ' '*(8-len(metric)), a95))
        
        ax[0, i].set_title(region, fontsize=18)
        ax[0, i].set_xlabel("Recall", fontsize=15)
        ax[0, i].set_ylabel("Precision", fontsize=15)
        ax[0, i].legend(loc=1, fontsize=12)
        ax[0, i].spines['right'].set_visible(False)
        ax[0, i].spines['top'].set_visible(False)
        ax[0, i].yaxis.set_ticks_position('left')
        ax[0, i].xaxis.set_ticks_position('bottom')

        ax[1, i].set_ylim(.95,1)
        ax[1, i].set_title(region, fontsize=18)
        ax[1, i].set_xlabel("Recall", fontsize=15)
        ax[1, i].set_ylabel("Precision", fontsize=15)
        ax[1, i].legend(loc=1, fontsize=12)
        ax[1, i].spines['right'].set_visible(False)
        ax[1, i].spines['top'].set_visible(False)
        ax[1, i].yaxis.set_ticks_position('left')
        ax[1, i].xaxis.set_ticks_position('bottom')
        
    plt.savefig("prauc.pdf")
    plt.show()
    fig.clf()


def plot_patient_roc(s):
    WEIGHTS = {"Average":
                   {"3' Intronic": 1, 
                    "5' Intronic": 1, 
                    "Exonic":      1,
                    "3' Core":     1, 
                    "5' Extended": 1,
                    "5' Core":     1},

               "Pathogenic":
                   {"3' Intronic": 1312, 
                    "5' Intronic":  160, 
                    "Exonic":       294,
                    "3' Core":     5174, 
                    "5' Extended": 2272, 
                    "5' Core":     6311},

               "Patient":
                   {"3' Intronic":  169, 
                    "5' Intronic":  176, 
                    "Exonic":       167,
                    "3' Core":        2, 
                    "5' Extended":   16, 
                    "5' Core":        3}}

    GRAPHS = ["Full", "High Sensitivity"]
    X_VALS = np.linspace(0, 1, 100)
    
    COMPS = ["SPIDEX", "CADD", "LINSIGHT", "EIGEN", "EIGEN_NONCODING", "TRAP", "CDTS"]

    n_figs = 0
    fig, axes = plt.subplots(len(WEIGHTS), len(GRAPHS), figsize=(16, 20), facecolor="white")
    for weights_name, weights in WEIGHTS.items():  
        mean_tpr = np.zeros((100,))
        comps_perf = {name: [np.zeros((100,)), 0.0]
                      for name in COMPS
                      if name != "EIGEN_NONCODING"}

        for region in weights.keys():
            test_X, test_Y = s.test_sets[region]
            preds = s.predict(region, test_X)[:,1]
            fpr, tpr, thresholds = roc_curve(test_Y, preds)
            mean_tpr += interp(X_VALS, fpr, tpr) * weights[region] / float(sum(weights.values()))

            ##################

            for i, name in enumerate(COMPS):
                scores = s.comp_sets[region][:,i]
                valid = scores != float('inf')
                if sum(valid) < len(scores)/2:
                    continue
                if name == 'EIGEN_NONCODING': name = 'EIGEN'
                Y = test_Y[valid]
                X = scores[valid]

                if name == 'SPIDEX' or name == 'CDTS':
                    X = -X

                fpr, tpr, thresholds = roc_curve(Y, X)
                # calculate the mean true positive rate
                comps_perf[name][0] += interp(X_VALS, fpr, tpr) * weights[region]
                comps_perf[name][1] += float(weights[region])

        for key in comps_perf:
            comps_perf[key][0] /= comps_perf[key][1]

        for plot_name in GRAPHS:
            ax = axes[n_figs/len(GRAPHS), n_figs % len(GRAPHS)]
            if plot_name == "Full":
                a = auc(np.linspace(0,1,100), mean_tpr)
            else:
                a = 20*(  auc(X_VALS, mean_tpr)
                        - auc(X_VALS, map(lambda x: min([x, .95]), mean_tpr)))
            ax.plot(X_VALS, mean_tpr, linestyle=linestyles["S-CAP"], c="k",
                    label = "{0}:{1} auc={2:.3f}".format('S-CAP', ' '*(8-len('S-CAP')), a))

            for key in comps_perf.keys():
                if plot_name == "Full":
                    a = auc(X_VALS, comps_perf[key][0])
                else:
                    a = 20*(  auc(X_VALS, comps_perf[key][0])
                            - auc(X_VALS, map(lambda x: min([x, .95]), comps_perf[key][0])))
                
                ds = len(X_VALS)/10 if plot_name == "Full" else len(X_VALS)/20
                ms = 4
                if markers[key]!='':
                    ax.plot(X_VALS, comps_perf[key][0], ls=linestyles[key], color="k")
                    ax.scatter(X_VALS[::ds], comps_perf[key][0][::ds], c = 'k', marker = markers[key],
                                  label = "{0}:{1} auc={2:.3f}".format(key, ' '*(8-len(key)), a))
                else:
                    ax.plot(X_VALS, comps_perf[key][0], ls=linestyles[key], color="k", 
                                  label = "{0}:{1} auc={2:.3f}".format(key, ' '*(8-len(key)), a))

                # ax.plot(X_VALS, comps_perf[key][0], c = "k", linestyle=linestyles[key], marker=markers[key], label = "{0}:{1} auc={2:.3f}".format(key, ' '*(8-len(key)), a))

            if plot_name=="Full":
                ax.set_ylim(0,1)
                ax.set_xlim(0,1)
            else:
                ax.set_ylim(.95,1)
                ax.set_xlim(0,1)
            ax.plot(X_VALS, X_VALS, c = 'k', ls='dotted')
            #ax.set_title("{} {} Weighted Averages".format(plot_name, weights_name), fontsize=15)
            ax.set_title("{} performance over all Splicing Regions".format(plot_name))
            ax.set_ylabel('TPR', fontsize=15)
            ax.set_xlabel('FPR', fontsize=15)
            
            # remove the top and right axis
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.yaxis.set_ticks_position('left')
            ax.xaxis.set_ticks_position('bottom')
            
            ax.legend(loc='best', fontsize=15)
            if plot_name == "High Sensitivity":
                ax.set_ylim(0.95, 1.0)
            n_figs += 1
    plt.savefig("patient_auc.pdf")
    fig.show()
    
def plot_roc(scap, regions):
    COMPS = ["SPIDEX", "CADD", "LINSIGHT", "EIGEN", "EIGEN_NONCODING", "TRAP", "CDTS"]
    fig, ax = plt.subplots(2, len(regions), figsize=(len(regions)*7, 11), facecolor="white")
    for i, region in enumerate(regions):
        test_X, test_Y = scap.test_sets[region]
        print(region, sum(test_Y==1), sum(test_Y==0))
        comps = scap.comp_sets[region]
        for idx, metric in [(None, 'S-CAP')]+list(enumerate(COMPS)):
            if metric == "S-CAP":
                scores = scap.predict(region, test_X)[:,1]
            elif metric == "SPIDEX" or metric == "CDTS":
                scores = -comps[:,idx]
            else:
                scores = comps[:,idx]
        
            #### Throws out inf
            valid = (scores != float('inf')) * (scores != -float('inf'))
            #print region, metric, sum(valid), len(scores)
            if sum(valid) < len(scores)/2:
                continue
                
            if metric == 'EIGEN_NONCODING': metric = 'EIGEN'
            
            Y = test_Y[valid]
            X = scores[valid]
            fpr, tpr, thresholds = roc_curve(Y, X)
            a = auc(fpr, tpr)
            a95 = 20*(a - auc(fpr, map(lambda x: min([x,.95]), tpr)))
            
            ds = len(fpr)/10
            ms = 4
            if markers[metric]!='':
                ax[0, i].plot(fpr, tpr, ls=linestyles[metric], color="k")
                ax[1, i].plot(fpr, tpr, ls=linestyles[metric], color="k")
                
                ax[0, i].scatter(fpr[::ds], tpr[::ds], c = 'k', marker = markers[metric],
                              label = "{0}:{1} auc={2:.3f}".format(metric, ' '*(8-len(metric)), a))
                ax[1, i].scatter(fpr[::ds], tpr[::ds], c='k', marker=markers[metric],
                              label = "{0}:{1} auc={2:.3f}".format(metric, ' '*(8-len(metric)), a95))
            else:
                ax[0, i].plot(fpr, tpr, ls=linestyles[metric], color="k", 
                              label = "{0}:{1} auc={2:.3f}".format(metric, ' '*(8-len(metric)), a))
                ax[1, i].plot(fpr, tpr, ls=linestyles[metric], color="k",
                             label = "{0}:{1} auc={2:.3f}".format(metric, ' '*(8-len(metric)), a95))
        
        ax[0, i].set_ylim(0,1)
        ax[0, i].set_xlim(0,1)
        ax[0, i].set_title(region, fontsize=18)
        ax[0, i].set_xlabel("FPR", fontsize=15)
        ax[0, i].set_ylabel("TPR", fontsize=15)
        ax[0, i].legend(loc=4, fontsize=12)
        ax[0, i].spines['right'].set_visible(False)
        ax[0, i].spines['top'].set_visible(False)
        ax[0, i].yaxis.set_ticks_position('left')
        ax[0, i].xaxis.set_ticks_position('bottom')

        ax[1, i].set_ylim(.95,1)
        ax[1, i].set_xlim(0,1)
        ax[1, i].set_title(region, fontsize=18)
        ax[1, i].set_xlabel("FPR", fontsize=15)
        ax[1, i].set_ylabel("TPR", fontsize=15)
        ax[1, i].legend(loc=2, fontsize=12)
        ax[1, i].spines['right'].set_visible(False)
        ax[1, i].spines['top'].set_visible(False)
        ax[1, i].yaxis.set_ticks_position('left')
        ax[1, i].xaxis.set_ticks_position('bottom')
        
    plt.savefig("auc.pdf")
    plt.show()
    fig.clf()

def read_variants(fn):
    return shuffle(pd.read_csv(fn, delimiter = '\t'))

def features_from_blocked(variants, blocked):
    return [feat for feat in list(variants.columns.values) if feat not in blocked]


def featurize(variants, features):
    feats = []
    for attribute in features:
        feats += [variants[attribute]]
    feats = np.vstack(feats).T.astype('float')
    feats[np.isinf(feats)] = 0
    return feats

def comparize(variants, comparisons):
    feats = []
    for attribute in comparisons:
        feats += [variants[attribute]]
    feats = np.vstack(feats).T.astype('float')
    return feats

#returns in format: auc, [scap objects, 1 for each in clfs]
def classify(path_train, benign_train, path_eval, benign_eval,
             features, comparisons, clfs, title, folds=5, regions=["All"]):

    #_, test_sets, comp_sets, chroms = get_traintestsets_from_data(path_eval, benign_eval, features, comparisons)
    train_sets, test_sets, comp_sets, chroms = get_traintestsets_from_data(path_train, benign_train, path_eval, benign_eval, features, comparisons)

    #################################################################################
    # Compute performances
    #regions = ["All", "3' Intronic", "3' Core", "Exonic", "5' Core", "5' Supporting", "5' Intronic"]
    models = {}
    name, clf = clfs
    for region in regions:
        """
        print "Training a model for " + region
        print (features[region])
        print (comparisons)
        """
        X_train, y_train = train_sets[region]
        X_test, y_test = test_sets[region]
        model = grid_search(X_train, X_train, X_test, y_train, y_train, y_test, clf, features[region], test_sets, comp_sets, region)
        models[region] = model
    s = Scap(title, features, models, test_sets, comp_sets, conf=.95)
    return s

def reverse_mapping(m):
    r = {}
    for k,vs in m.iteritems():
        for v in vs:
            r[v] = k
    return r

def interpret(scap, feature_groupings):
    
    group_features = reverse_mapping(feature_groupings)
        
    for region, model in scap.models.iteritems():
        feature_importances = defaultdict(int)

        importances = model.feature_importances_
        indices = np.argsort(importances)[::-1]

        test_X, test_Y = scap.test_sets[region]

        # Print the feature ranking
        print("Feature ranking in %s region:"%(region))

        for f in range(test_X.shape[1]):
            group_name = group_features.get(scap.feats[region][indices[f]], scap.feats[region][indices[f]])
            feature_importances[group_name] += importances[indices[f]]
            
        sorted_importances = sorted([(f, importance) for f, importance in feature_importances.iteritems()], key=lambda x: x[1], reverse=True)

        
        for f, importance in sorted_importances:
            print("feature %s %f" % (f, importance))

        # Plot the feature importances of the forest
        plt.figure()
        plt.title("Feature importances")
        plt.bar(range(test_X.shape[1]), importances[indices], color="r", align="center")
        plt.xticks(range(test_X.shape[1]), indices)
        plt.xlim([-1, test_X.shape[1]])
        plt.show()
        
def cross_validation_old(X, X_eval, Y, Y_eval,  model, title, features):
    mean_tpr, mean_fpr = 0.0, np.linspace(0, 1, 100)
    mean_tnr, mean_fnr = 0.0, np.linspace(0, 1, 100)
    folds_aucs = []
    folds_model = []
    
    for idx in range(len(X)):
        X_train = np.vstack([x for i, x in enumerate(X) if i != idx])
        y_train = np.hstack([x for i, x in enumerate(Y) if i != idx])
        chroms = 1
        #print X_train.shape
        
        m = model.fit(X_train, y_train)
        probs = m.predict_proba(X_eval[idx])

        # calculate the mean true positive rate
        fpr, tpr, thresholds = roc_curve(Y_eval[idx], probs[:,1])
        print "fold number {}".format(idx)
        print "gene_cv auc: {}".format(roc_auc_score(Y_eval[idx], probs[:,1]))
        
        mean_tpr += interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0.0

        # calculate the mean true negative rate
        tnr, fnr = calculate_tnr_fnr(probs[:,1], Y_eval[idx])
        mean_tnr += interp(mean_fnr, fnr, tnr)
        mean_tnr[0] = 0.0
        
        a = auc(fpr, tpr)
        a95 = 20*(a-auc(fpr, map(lambda x: min([x, .95]), tpr)))
        
        folds_aucs += [(a, a95)]
        #folds_model.append(m)
        #print Scap(title, features, m, X_eval[idx], Y_eval[idx], median_idx=-1, conf=.95)
        
        folds_model.append(ScapOld(title, features, copy.copy(m), X_eval[idx], Y_eval[idx], conf=.95))

    median = np.median(folds_aucs, axis=0)[0]
    median_idx = [index for index, item in enumerate(folds_aucs) if item[0]==median][0]
    median_model = folds_model[median_idx]
    median_model.set_median_idx(median_idx)
    mean_tpr /= len(X)
    mean_tnr /= len(X)
    
    print folds_aucs
    print median_idx
    print folds_model
    print median_model
    
    assert median_model.auc == folds_aucs[median_idx][0]

    #np.std axis=0, xth element of every array in xth index
    #np.std axis=1, first array
    #return fpr, tpr, fnr, tnr, auc_std, auc95_std, scap object
    return np.linspace(0,1,100), mean_tpr, np.linspace(0,1,100), mean_tnr, np.std(folds_aucs, axis=0)[0], np.std(folds_aucs, axis=0)[1], median_model

def classify_old(path_train, benign_train, path_eval, benign_eval,
             features, comparisons, clfs, title):

    # Break up chromosomes based on benign_eval.
    chroms = partition(counts_tuples(path_eval['gene_common_name'],
                                     np.hstack([path_train['gene_common_name'],
                                                benign_train['gene_common_name'],
                                                benign_eval['gene_common_name']])), 5)
    
    #for idx in sorted(set(chroms.values())):
    #    print [chrom for chrom, part in chroms.items() if part == idx]
    
    # Featurize evaluation set.
    path_eval_feats   = featurize(path_eval, features)
    benign_eval_feats = featurize(benign_eval, features)
    path_eval_comps   = comparize(path_eval, comparisons)
    benign_eval_comps = comparize(benign_eval, comparisons)
 
    # ... and the training set.
    path_feats   = featurize(path_train, features)
    benign_feats = featurize(benign_train, features)

    # Distribute into folds by chromosome.
    X_train, y_train = distribute(path_feats, path_train['gene_common_name'],
                                  benign_feats, benign_train['gene_common_name'], chroms)
    X_eval, y_eval = distribute(path_eval_feats, path_eval['gene_common_name'],
                                benign_eval_feats, benign_eval['gene_common_name'], chroms)
    C, _y_eval = distribute(path_eval_comps, path_eval['gene_common_name'],
                            benign_eval_comps, benign_eval['gene_common_name'], chroms)

    assert np.all(np.hstack(y_eval) == np.hstack(_y_eval)), "Comps not same as classifications"

    #print map(len, y_train), map(len, y_eval)
    #print map(sum, y_train), map(sum, y_eval)
 
    #################################################################################
    # Compute performances
    clf_performances = []
    scaps = []
    for name, clf in clfs.items():
        fpr, tpr, fnr, tnr, auc_std, auc95_std, model, = cross_validation_old(X_train, X_eval, y_train, y_eval, clf, title, features)
        clf_performances += [(fpr, tpr, name, auc_std, auc95_std)]
        scaps.append(model)
        
    comp_performances = []
    C_tot, y_tot = np.vstack(C), np.hstack(y_eval)

    for i, attr in enumerate(comparisons):
        score = C_tot[:, i]
        _score = score[score != float('inf')]
        _y = y_tot[score != float('inf')]
        if 2*_y.shape[0] < y_tot.shape[0]:
            comp_performances += [(None, None, None, None, None)]
            continue
        if attr == 'SPIDEX':
            _score = 100 - _score
        if attr == 'RVIS':
            _score = -_score
        fpr, tpr, thresholds = roc_curve(_y, _score, pos_label = 1)
        
        #calculate std to compare to our cv-folds std
        aucs, aucs95 = [], []
        for idx in range(len(C)):
            C_fold = C[idx]
            y_fold = y_eval[idx]
            score_fold = C_fold[:, i]
            _score_fold = score_fold[score_fold != float('inf')]
            _y_fold = y_fold[score_fold != float('inf')]
            
            if attr == 'SPIDEX':
                _score_fold = 100 - _score_fold
            if attr == 'RVIS':
                _score_fold = -_score_fold
            
            fpr_fold, tpr_fold, thresholds = roc_curve(_y_fold, _score_fold, pos_label = 1)
            
            #####TODO#####
            #loop through saved scap objects
                #if fold is median fold
                    #add the whole comparisons matrix to object
            for s in scaps:
                #print "curr: {}".format(idx)
                #print "median idx: {}".format(s.median_idx)
                if idx == s.median_idx:
                    #print C_fold
                    s.set_comps(C_fold)
            
            aucs.append(auc(fpr_fold, tpr_fold))
            aucs95.append(20*(aucs[idx]-auc(fpr_fold, map(lambda x: min([x, .95]), tpr_fold))))

        #print attr, aucs, np.std(aucs)    
        comp_performances += [(fpr, tpr, attr, np.std(aucs), np.std(aucs95))]

    ################################################################################
    # ROC Plots.
    auROC = []
    for fpr, tpr, name, auc_std, auc95_std in clf_performances:
        a = auc(fpr, tpr)
        if name == 'Boosting Trees':
            auROC += [a]
        plt.plot(fpr, tpr, label = "{0}: auc={1:.3f}, std={2:.3f}".format(name, a, auc_std))
    for fpr, tpr, attr, auc_std, auc95_std in comp_performances:
        if fpr is None: continue
        a = auc(fpr, tpr)
        plt.plot(fpr, tpr, label = "{0}: auc={1:.3f}, std={2:.3f}".format(attr, a, auc_std), ls='dashed')
    
    plt.title("{}: p={},b={}".format(title, sum(y_tot == 1), sum(y_tot == 0)))
    plt.ylabel('TPR')
    plt.xlabel('FPR')
    plt.legend(loc='best', fontsize='x-small')
    plt.show()

    ##################################################################################
    # High sensitivity region ROC Plots.
    
    for fpr, tpr, name, auc_std, auc95_std in clf_performances:
        a = auc(fpr, tpr)
        b = auc(fpr, map(lambda x: min([x, .95]), tpr))
        plt.plot(fpr, tpr, label = "{0}: auc={1:.3f}, std={2:.3f}".format(name, 20*(a-b), auc95_std))
    for fpr, tpr, attr, auc_std, auc95_std in comp_performances:
        if fpr is None: continue
        a = auc(fpr, tpr)
        b = auc(fpr, map(lambda x: min([x, .95]), tpr))
        plt.plot(fpr, tpr, label = "{0}: auc={1:.3f}, std={2:.3f}".format(attr, 20*(a-b), auc95_std), ls='dashed')
        
    plt.ylim(0.95, 1.0)
    plt.title("{}: p={},b={}".format(title, sum(y_tot == 1), sum(y_tot == 0)))
    plt.ylabel('TPR')
    plt.xlabel('FPR')
    plt.legend(loc='best', fontsize='x-small')
    plt.show()
    return scaps #auROC, scaps
