#!/bin/bash

in_vcf=$1
base_dir=$2
variant_type=$3

scripts_dir=/cluster/u/kjag/splicing_classifier/scripts

filename=$(echo $in_vcf | rev | cut -d/ -f1 | rev)
type=$(echo $filename | cut -d. -f2)
split_dir=$base_dir/split_$type/
par_dir=$base_dir/par_$type/

#CLEAN up
rm -rf $split_dir
rm -rf $par_dir

echo $split_dir
echo $par_dir

#make directory structure
mkdir -p $split_dir
mkdir -p $par_dir


header=$base_dir/$filename.header
bcftools view -h $in_vcf > $header
bcftools view -H $in_vcf > $base_dir/$filename.tmp

splitFile $base_dir/$filename.tmp 5000 $split_dir -head=$header

find $split_dir -type f | xargs -i echo "$scripts_dir/annotate_features.sh {} $variant_type" > $par_dir/joblist

cd $par_dir
para make $par_dir/joblist -ram=8g
cd -


bcftools view -h $split_dir/01.features > $header
cat $split_dir/*.features.filter | grep -v "#" | cat $header /dev/stdin > $base_dir/hg19.$type.features.vcf
#cat $split_dir/*.features.filter.tsv | grep -v "CHROM" | cat <(head -1 $split_dir/01.features.filter.tsv) /dev/stdin > $base_dir/hg19.$type.features.tsv
cat $split_dir/*.features.filter.all.tsv | grep -v "CHROM" | cat <(head -1 $split_dir/01.features.filter.all.tsv) /dev/stdin > $base_dir/hg19.$type.features.tsv

#cat $split_dir/*.features.filter | grep -v "#" | cat $header /dev/stdin > $in_vcf.features.vcf

# Clean up
rm -rf $split_dir
rm -rf $par_dir
rm $base_dir/$filename.tmp
rm $header
