"""
This script adds sequence based features to a variant tsv file.

See code for command line arguments.
"""

import sys
import numpy as np
import pandas
from MaxEntScan.maxentscan import maxentscan

def ref_five(v, genome):
    refseqs, altseqs = [], []
    for i, (chrom, pos, shift, strand, alt, ref) in enumerate(zip(v['CHROM'],
                                                                  v['POS'],
                                                                  v["5'Dist"],
                                                                  v['strand'],
                                                                  v['ALT'],
                                                                  v['REF'])):
        chrom = 'chr'+str(chrom)
        ss = pos + (-shift-1 if strand == '+' else shift)
        if strand == '+':
            begin, end = ss-3, ss+6
        else:
            begin, end = ss-6, ss+3
        refseq = genome.get_seq(chrom, begin, end, strand)
        if 'N' in refseq: refseq = refseq.replace('N', 'A')
        refseqs += [refseq]
        if -3 <= shift < 6:
            altseqs += [refseq[:shift+3]
                        + (alt if strand == '+' else genome.revcomp(alt))
                        + refseq[shift+4:]]
        else:
            altseqs += [refseq]
    refscores = maxentscan(refseqs, True)
    altscores = maxentscan(altseqs, True)
    v['MaxEntScanRefFive'] = pandas.Series(refscores, index = v.index)
    v['MaxEntScanAltFive'] = pandas.Series(altscores, index = v.index)
    v['MaxEntScanDiffFive'] = pandas.Series(altscores-refscores, index = v.index)

def ref_three(v, genome):
    refseqs, altseqs = [], []
    for i, (chrom, pos, shift, strand, alt, ref) in enumerate(zip(v['CHROM'],
                                                                  v['POS'],
                                                                  v["3'Dist"],
                                                                  v['strand'],
                                                                  v['ALT'],
                                                                  v['REF'])):
        chrom = 'chr'+str(chrom)
        ss = pos + (-shift if strand == '+' else shift-1)
        if strand == '+':
            begin, end = ss-20, ss+3
        else:
            begin, end = ss-3, ss+20
        refseq = genome.get_seq(chrom, begin, end, strand)
        if 'N' in refseq: refseq = refseq.replace('N', 'A')
        refseqs += [refseq]
        if -20 < shift <= 3:
            altseqs += [refseq[:shift-4]
                        + (alt if strand == '+' else genome.revcomp(alt))
                        + refseq[shift-3:]]
        else:
            altseqs += [refseq]
    refscores = maxentscan(refseqs, False)
    altscores = maxentscan(altseqs, False)
    v['MaxEntScanRefThree'] = pandas.Series(refscores, index = v.index)
    v['MaxEntScanAltThree'] = pandas.Series(altscores, index = v.index)
    v['MaxEntScanDiffThree'] = pandas.Series(altscores-refscores, index = v.index)

def crypt_three(v, genome):
    refs, alts, dists, indices = [], [], [], []
    for idx, (chrom, pos, shift, strand, alt, ref) in enumerate(zip(v['CHROM'],
                                                                    v['POS'],
                                                                    v["3'Dist"],
                                                                    v['strand'],
                                                                    v['ALT'],
                                                                    v['REF'])):
        chrom = 'chr'+str(chrom)
        refseq = genome.get_seq(chrom, pos-23, pos+22, strand)
        if 'N' in refseq: refseq = refseq = refseq.replace('N', 'A')
        
        altseq = (refseq[:22]
                  + (alt if strand == '+' else genome.revcomp(alt))
                  + refseq[23:])
        for i in range(0, len(refseq)-22):
            crypt_dist = -shift-i+3
            if not crypt_dist: continue
            refsubseq = refseq[i:i+23]
            altsubseq = altseq[i:i+23]
            if altsubseq[18:20] != 'AG': continue
            alts += [altsubseq]
            refs += [refsubseq]
            indices += [idx]
            dists += [crypt_dist]

    if refs:
        assert alts
        assert indices
        refscores = maxentscan(refs, False)
        altscores = maxentscan(alts, False)
    else:
        assert not alts
        assert not indices
        refscores = np.array([])
        altscores = np.array([])
    diffs = altscores - refscores
    
    deltacrypt = np.zeros((v.shape[0],))
    bestcrypt  = np.zeros((v.shape[0],))
    distcrypt  = np.zeros((v.shape[0],))
    for idx, diff, alt, dist in zip(indices, diffs, altscores, dists):
        if alt > bestcrypt[idx]:
            bestcrypt[idx] = alt
            deltacrypt[idx] = diff
            distcrypt[idx] = dist
            
    v['DeltaCrypt3'] = pandas.Series(deltacrypt, index = v.index)
    v['BestCrypt3'] = pandas.Series(bestcrypt, index = v.index)
    v['DistCrypt3'] = pandas.Series(distcrypt, index = v.index)
    v['DistCryptMod3'] = pandas.Series((distcrypt % 3 == 0).astype(int), index = v.index)

def crypt_five(v, genome):
    refs, alts, dists, indices = [], [], [], []
    for idx, (chrom, pos, shift, strand, alt, ref) in enumerate(zip(v['CHROM'],
                                                               v['POS'],
                                                               v["5'Dist"],
                                                               v['strand'],
                                                               v['ALT'],
                                                               v['REF'])):
        chrom = 'chr'+str(chrom)
        refseq = genome.get_seq(chrom, pos-9, pos+8, strand)
        if 'N' in refseq: refseq = refseq.replace('N', 'A')
        altseq = (refseq[:8]
                  + (alt if strand == '+' else genome.revcomp(alt))
                  + refseq[9:])
        for i in range(0, len(refseq)-8):
            crypt_dist = shift-5+i
            if not crypt_dist: continue
            refsubseq = refseq[i:i+9]
            altsubseq = altseq[i:i+9]
            if altsubseq[3:5] != 'GT': continue
            alts += [altsubseq]
            refs += [refsubseq]
            indices += [idx]
            dists += [crypt_dist]
    
    if refs:
        assert alts
        assert indices
        refscores = maxentscan(refs, True)
        altscores = maxentscan(alts, True)
    else:
        assert not alts
        assert not indices
        refscores = np.array([])
        altscores = np.array([])
    diffs = altscores - refscores
    
    deltacrypt = np.zeros((v.shape[0],))
    bestcrypt  = np.zeros((v.shape[0],))
    distcrypt  = np.zeros((v.shape[0],))
    for idx, diff, alt, dist in zip(indices, diffs, altscores, dists):
        if alt > bestcrypt[idx]:
            bestcrypt[idx] = alt
            deltacrypt[idx] = diff
            distcrypt[idx] = dist 
            
    v['DeltaCrypt5'] = pandas.Series(deltacrypt, index = v.index)
    v['BestCrypt5'] = pandas.Series(bestcrypt, index = v.index)
    v['DistCrypt5'] = pandas.Series(distcrypt, index = v.index)
    v['DistCryptMod5'] = pandas.Series((distcrypt % 3 == 0).astype(int), index = v.index)

##############################################################################

def anno_bp(v, bps):
    scores = []
    for chrom, pos, strand in zip(v['CHROM'], v['POS'], v['strand']):
        chrom = 'chr'+str(chrom)
        if chrom in bps and pos in bps[chrom]:
            scores += [bps[chrom][pos]]
        else:
            scores += [0]
    print sum(scores)
    v['BP'] =  pandas.Series(np.array(scores), index = v.index)

##############################################################################

from sklearn.externals import joblib

EXONS =    ['hg19', 'panTro4', 'gorGor3', 'ponAbe2', 'nomLeu3', 'rheMac3', 'macFas5',
            'papHam1', 'chlSab1', 'calJac3', 'saiBol1', 'otoGar3', 'tupChi1', 'speTri2',
            'jacJac1', 'micOch1', 'criGri1', 'mesAur1', 'mm10', 'rn5', 'hetGla2',
            'cavPor3', 'chiLan1', 'octDeg1', 'oryCun2', 'ochPri3', 'susScr3', 'vicPac2',
            'camFer1', 'turTru2', 'orcOrc1', 'panHod1', 'bosTau7', 'oviAri3', 'capHir1',
            'equCab2', 'cerSim1', 'felCat5', 'canFam3', 'musFur1', 'ailMel1', 'odoRosDiv1',
            'lepWed1', 'pteAle1', 'pteVam1', 'myoDav1', 'myoLuc2', 'eptFus1', 'eriEur2',
            'sorAra2', 'conCri1', 'loxAfr3', 'eleEdw1', 'triMan1', 'chrAsi1', 'echTel2',
            'oryAfe1', 'dasNov3', 'monDom5', 'sarHar1', 'macEug2', 'ornAna1', 'falChe1',
            'falPer1', 'ficAlb2', 'zonAlb1', 'geoFor1', 'taeGut2', 'pseHum1', 'melUnd1',
            'amaVit1', 'araMac1', 'colLiv1', 'anaPla1', 'galGal4', 'melGal1', 'allMis1',
            'cheMyd1', 'chrPic1', 'pelSin1', 'apaSpi1', 'anoCar2', 'xenTro7', 'latCha1',
            'tetNig2', 'fr3', 'takFla1', 'oreNil2', 'neoBri1', 'hapBur1', 'mayZeb1', 'punNye1',
            'oryLat2', 'xipMac1', 'gasAcu1', 'gadMor1', 'danRer7', 'astMex1', 'lepOcu1', 'petMar2']

def add_exon_pc(v, k, pca):
    pc = pca.transform(v[EXONS].as_matrix())
    for i in range(k):
        v["EXON_IMPORT{}".format(i)] = pandas.Series(pc[:, i], index = v.index)

################################################################

BASES = {'A': '00', 'C': '01', 'G': '10', 'T': '11', 'N':'00'}

def kmerize(seq, k):
    binary_seq = ''.join(map(lambda x: BASES[x], seq))
    count = np.zeros((4**k,), dtype = int)
    for i in range(0, len(seq) - k + 1):
        count[int(binary_seq[i*2:(i+k)*2], 2)] += 1
    return count

def spectrum(variant, genome, K = 3):
    chrom, pos, strand, ref, alt = variant
    pos += -1  # 1-index to 0-index
    alt = alt if strand == '+' else ''.join(map(genome.revcomp, alt[::-1]))
    refseq = genome.get_seq('chr'+str(chrom), int(pos)-K, int(pos)+K+len(ref), strand)
    altseq = refseq[:K] + alt + refseq[-K:]
    return kmerize(refseq, K) - kmerize(altseq, K)

def spectrum_interp(x):
    m = {'00': 'A', '01':'C', '10':'G', '11':'T'}
    b = "{0:b}".format(x).zfill(6)
    return ''.join([m[b[ch:ch+2]] for ch in range(0, 6, 2)])

#################################################################
#################################################################
# Load Data
v = pandas.read_csv(sys.argv[1], delimiter = '\t', dtype={'CHROM': str})
print v.shape

import pickle
with open(sys.argv[2], 'rb') as g:
    genome = pickle.load(g)
print 'loaded genome'

###################################################################
# BP feature
#bps = {}
#with open(sys.argv[3]) as fp:
#    for line in fp:
#        chrom, bp, score = line.strip().split()
#        chrom = 'chr'+chrom
#        if chrom not in bps: bps[chrom] = {}
#        bps[chrom][int(bp)] = float(score)
#anno_bp(v, bps)
#print 'added branch point score'

#################################################################
# EXON PCA
pca = joblib.load(sys.argv[3])
add_exon_pc(v, 6, pca)
v = v.drop(EXONS, axis = 1)
print 'replaced exon identiies with projection on PCs'

##################################################################
# MaxEntScan Based Scores
ref_five(v, genome)
print 'ref_five done'
ref_three(v, genome)
print 'ref_three done'
crypt_five(v, genome)
print 'crypt_five done'
crypt_three(v, genome)
print 'crypt_three done'

# Composite Cryptic site Features
v['RefCryptDiffFive'] =  v['BestCrypt5'] - v['MaxEntScanAltFive']
v['RefCryptDiffThree'] = v['BestCrypt3'] - v['MaxEntScanAltThree']

v['RefCryptDiffFiveFS'] = (1-v['DistCryptMod5'])*v['RefCryptDiffFive']
v['RefCryptDiffFiveNOFS'] = (v['DistCryptMod5'])*v['RefCryptDiffFive']
v['RefCryptDiffThreeFS'] = (1-v['DistCryptMod3'])*v['RefCryptDiffThree']
v['RefCryptDiffThreeNOFS'] = (v['DistCryptMod3'])*v['RefCryptDiffThree']

#########################################################
# Spectrum Kernel
spec = np.vstack([spectrum(variant, genome, K = 3)
                  for variant in zip(v['CHROM'],
                                     v['POS'],
                                     v['strand'],
                                     v['REF'],
                                     v['ALT'])])
for i in range(spec.shape[1]):
    v[spectrum_interp(i)] = pandas.Series(spec[:, i], index = v.index)
print 'Computed spectrum kernel'

##########################################################

v.to_csv(sys.argv[4], sep = '\t', index = False)
