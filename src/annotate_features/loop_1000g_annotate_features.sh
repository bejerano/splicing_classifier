in_dir=$1
out_dir=$2
variant_type=$3

scripts_dir=/cluster/u/kjag/splicing_classifier/scripts

split_dir=$out_dir/split_1000g
par_dir=$out_dir/par_1000g

#CLEAN up
rm -rf $split_dir
rm -rf $par_dir

#make directory structure
mkdir -p $split_dir
mkdir -p $par_dir

#find $in_dir -type f -name "*.vcf.gz" | shuf -n20 > $out_dir/random_kgp_ids

#cat $out_dir/random_kgp_ids | while read line; do 
find $in_dir -type f -name "*.vcf.gz" | while read line; do
kgp_id=$(echo $line | rev | cut -d/ -f1 | rev | cut -d. -f1)
echo $kgp_id

header=$out_dir/$kgpd_id'.header'

bcftools view -h $in_dir/$kgp_id'.vcf.gz' > $header
bcftools view -H $in_dir/$kgp_id'.vcf.gz' > $out_dir/$kgp_id'.tmp'

splitFile $out_dir/$kgp_id'.tmp' 5000 $split_dir/$kgp_id'.' -head=$header

find $split_dir -type f -name "$kgp_id.*" | xargs -i echo "$scripts_dir/annotate_features.sh {} $variant_type" >> $par_dir/joblist

rm $out_dir/$kgp_id'.tmp'
rm $header

done

cd $par_dir
para make $par_dir/joblist -ram=8g
cd -


#cat $out_dir/random_kgp_ids | while read line; do
find $in_dir -type f -name "*.vcf.gz" | while read line; do
kgp_id=$(echo $line | rev | cut -d/ -f1 | rev | cut -d. -f1)
header=$out_dir/$kgpd_id'.header'

bcftools view -h $split_dir/$kgp_id'.01.features' > $header
cat $split_dir/$kgp_id.*.features.filter | grep -v "#" | cat $header /dev/stdin > $out_dir/$kgp_id.features.vcf
#cat $split_dir/$kgp_id.*.features.filter.tsv | grep -v "CHROM" | cat <(head -1 $split_dir/$kgp_id'.01.features.filter.tsv') /dev/stdin > $out_dir/$kgp_id.features.tsv
cat $split_dir/$kgp_id.*.features.filter.all.tsv | grep -v "CHROM" | cat <(head -1 $split_dir/$kgp_id'.01.features.filter.all.tsv') /dev/stdin > $out_dir/$kgp_id.features.tsv



rm $header
done

# Clean up
rm -rf $split_dir
rm -rf $par_dir
