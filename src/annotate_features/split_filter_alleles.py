"""
Remove all common or protein altering variants.

Input is a valid vcf file with at least ALFQ and SE
fields. Can be gzipped, uncompressed or stdin.

- Common is defined as having a max ALFQ > THRESH.
- Protein altering is defined as having a semantic
  effect in EFFECTS for any transcript.
- Multi-allelic variants are separated into one
  variant per line.
"""

EFFECTS = ['stoploss', 'stopgain', 'nonsynonymous']
THRESH  = 0.01


def is_splicing(info, pos, strand):
    """
    return true if the variant is in the splicing region.
    this means in eithor the 5' intronic, 5' supporting, 
    5' core, exonic, 3' core or 3' intronic regions.
    """
    exon_features = [entry for entry in info if entry[:13]=="EXON_FEATURES"]
    if exon_features == []:
        return False
    chrom, start, end = exon_features[0].split("=")[1].split(",")[0].split(":")[0].split("_")
    start = start
    end = end
    return is_core(start, end, pos) or is_extended(start, end, pos) or is_splice_intronic(start, end, pos) or is_5exonic_core(start, end, pos, strand) or is_exonic(start, end, pos)

def is_rare(info):
    """
    return true iff the max gnomad 
    alt allele frequency is less than 
    or equal to THRESH.
    
    info is a list of strings,
    corresponding to a ';' split
    on the vcf INFO field.
    
    we expect there to be a ALFQ
    field, if there is not, we
    throw an exception.
    """
    gnomad_alfq = [entry for entry in info if entry[:11] == 'GNOMAD_ALFQ']
    exac_alfq = [entry for entry in info if entry[:4] == "ALFQ"]
    if len(gnomad_alfq) != 1 or len(exac_alfq) != 1:
        print gnomad_alfq, exac_alfq, info
        raise ValueError
    exac_alfqmax = exac_alfq[0].split("=")[1].split(",")[1].split(":")[2]

    alfqmax = max([0] + [float(f) for f in gnomad_alfq[0].split("=")[1].split(",")[1].split(":")[1:] if f != "."] + [float(exac_alfqmax)])
    return alfqmax <= THRESH

def is_synonymous(info):
    """
    returns true if info field indicates that this
    variant causes no change in a protien coding
    transcript.
    
    if there is no SE field, we return false
    """
    se = [entry for entry in info if entry[:2] == 'SE']
    if len(se) != 1: return False
    se = ''.join(transcript for transcript in se[0].split('=')[1].split(',')
                 if transcript[0] == '1')
    return not any(effect in se for effect in EFFECTS)

def is_blacklisted(chrom, pos, variant_class, hgmd_variants):
    if variant_class == 'pathogenic':
        return False
    else:
        return (chrom, pos) in hgmd_variants

def load_hgmd(filename):
    hgmd_variants = set([])
    for line in open(filename):
        if line.startswith("#"):
            continue
        tokens = line.strip().split("\t")
        hgmd_variants.add((tokens[0],tokens[1]))
    return hgmd_variants

##############################################################
def is_core(start, end, pos):
    if (int(pos) > int(end)) and (int(pos) <= int(end) + 2):
        return True
    elif (int(pos) <= int(start)) and (int(pos) > int(start) - 2):
        return True
    else:
        return False

def is_extended(start, end, pos):
    if (int(pos) > int(end)+2) and (int(pos) <= int(end) +6):
        return True
    elif (int(pos) <= int(start)-2) and (int(pos) > int(start) - 6):
        return True
    else:
        return False

def is_splice_intronic(start, end, pos):
    if (int(pos) > int(end)+6) and (int(pos) <= int(end) + 50):
        return True
    elif (int(pos) <= int(start)-6) and (int(pos) > int(start) - 50):
        return True
    else:
        return False

def is_5exonic_core(start, end, pos, strand):
    if strand == "+" and int(end) - 5 < pos <= int(end) :
        return True
    elif strand == "-" and int(start) < pos <= int(start) + 5:
        return True
    else:
        return False

def is_exonic(start, end, pos):
    if start < pos < end:
        return True
    else:
        return False

def resolve_exon(val, chrom, pos):
    keep = None
    smallest_exon_len = float("inf")
    found_core = False
    found_extended = False
    found_5exonic_core = False
    found_splice_intronic = False
    for exon_info in val.split(","):
        exon_id = exon_info.split(":")[0]
        chrom, start, end = exon_id.split("_")
        strand = exon_info.split(":")[1]
        exon_len = int(end) - int(start)
        if is_core(start, end, pos):
            if found_core==False or exon_len < smallest_exon_len:
                smallest_exon_len = exon_len
                keep = exon_info
                found_core = True
        elif is_extended(start, end, pos) and (not found_core):
            if found_extended==False or exon_len < smallest_exon_len:
                smallest_exon_len = exon_len
                keep = exon_info
                found_extended = True
        elif is_5exonic_core(start, end, pos, strand) and (not found_extended) and (not found_core):
            if found_5exonic_core==False or exon_len < smalles_exon_len:
                smallest_exon_len = exon_len
                keep = exon_info
                found_5exonic_core = True
        elif is_splice_intronic(start, end, pos) and (not found_extended) and (not found_core) and (not found_5exonic_core):
            if found_splice_intronic == False or exon_len < smallest_exon_len:
                smallest_exon_len = exon_len
                keep = exon_info
                found_splice_intronic = True
        elif (smallest_exon_len > exon_len) and (not found_5exonic_core) and (not found_core) and (not found_extended) and (not found_splice_intronic):
            smallest_exon_len = exon_len
            keep = exon_info
    return keep


def resolve_transcript(val, chrom, pos, strand):
    keep = None
    smallest_exon_len = float("inf")
    found_core = False
    found_extended = False
    found_5exonic_core = False
    found_splice_intronic = False
    for exon_info in val.split(","):
        exon_id = exon_info.split(":")[1]
        chrom, start, end = exon_id.split("_")
        exon_len = int(end) - int(start)
        if is_core(start, end, pos):
            if found_core==False or exon_len < smallest_exon_len:
                smallest_exon_len = exon_len
                keep = exon_info
                found_core = True
        elif is_extended(start, end, pos) and (not found_core):
            if found_extended==False or exon_len < smallest_exon_len:
                smallest_exon_len = exon_len
                keep = exon_info
                found_extended = True
        elif is_5exonic_core(start, end, pos, strand) and (not found_extended) and (not found_core):
            if found_5exonic_core==False or exon_len < smalles_exon_len:
                smallest_exon_len = exon_len
                keep = exon_info
                found_5exonic_core = True
        elif is_splice_intronic(start, end, pos) and (not found_extended) and (not found_core) and (not found_5exonic_core):
            if found_splice_intronic == False or exon_len < smallest_exon_len:
                smallest_exon_len = exon_len
                keep = exon_info
                found_splice_intronic = True
        elif (smallest_exon_len > exon_len) and (not found_5exonic_core) and (not found_core) and (not found_extended) and (not found_splice_intronic):
            smallest_exon_len = exon_len
            keep = exon_info
    return keep


def main(IN, OUT, VARIANT_CLASS):
    splice_region, nonsynonymous, common, total, written = 0, 0, 0, 0, 0
    hgmd_variants = load_hgmd("/cluster/u/kjag/hgmd2017/HGMD_PRO_2017.1_hg19.vcf")
    for line in IN:
        # Write header lines as they appear in input
        if line[0] == '#':
            OUT.write(line)
            continue

        # Write line(s) of body, considering all alleles independently.
        chrom, pos, ID, ref, alt, qual, filt, info = line.strip().split('\t')[:8]
        info_fields = []
        strand = ""

        for entry in info.split(";"):
            key, val = entry.split("=")
            if key == "EXON_FEATURES" or key == "LEFT_EDGE_EXON_FEATURES" or key == "RIGHT_EDGE_EXON_FEATURES":
                keep = resolve_exon(val, chrom, pos)
                strand = keep.split(":")[1]
                entry = "=".join([key, keep])
                info_fields += [entry]
                continue

            if key=="PHYLOP100WAY" or key == "PHASTCONS100WAY" or key=="LINSIGHT" or key == "PHCONS_PLACENTAL" or key == "PHCONS_PRIMATES" or key == "PHCONS_VERTEBRATES" or key == "PHYLOP_VERTEBRATES" or key == "PHYLOP_PLACENTAL" or key == "PHYLOP_PRIMATES":
                keep = max([float(v) for v in val.split(",")])
                entry = "=".join([key,str(keep)])
                info_fields += [entry]
                continue

            if key == "TRANSCRIPT_FEATURES":
                keep = resolve_transcript(val, chrom, pos, strand)
                entry = "=".join([key, keep])
                info_fields += [entry]
                continue
            if key == "regional_constraint":
                keep = min([float(v) for v in val.split(",")])
                entry = "=".join([key, str(keep)])
                info_fields += [entry]
                continue

            # Write fields with no allelic dependence
            if (len(val.split(',')) == 1
                or not any(map(lambda x: x[0] in ['0', '1', '2'], val.split(',')))):
                info_fields += [entry]
                continue
            
        for i, allele in enumerate(alt.split(',')):
            i = i+1
            allele_info = []
            for entry in info.split(';'):
                key, val = entry.split('=')
                if key == "regional_constraint" or key == "TRANSCRIPT_FEATURES" or key == "EXON_FEATURES" or key == "LEFT_EDGE_EXON_FEATURES" or key == "RIGHT_EDGE_EXON_FEATURES" or key=="PHYLOP100WAY" or key == "PHASTCONS100WAY" or key=="LINSIGHT" or key == "PHCONS_PLACENTAL" or key == "PHCONS_PRIMATES" or key == "PHCONS_VERTEBRATES" or key == "PHYLOP_VERTEBRATES" or key == "PHYLOP_PLACENTAL" or key == "PHYLOP_PRIMATES" or (len(val.split(',')) == 1 or not any(map(lambda x: x[0] in ['0', '1', '2'], val.split(',')))):
                    continue
 
                # For fields with allelic dependence,
                # ... only extract relevant fields
                # ... set allele ID to 1.
                field = []
                for el in val.split(','):
                    k = int(el.split(':')[0])
                    if i == k: # ALT
                        el = '1' + el[1:]
                        field += [el]
                    elif 0 == k: # ... OR REF.
                        field += [el]
                allele_info += ["{}={}".format(key, ','.join(field))]

            splicing, rare, synonymous, blacklist = is_splicing(info_fields, pos, strand), is_rare(allele_info), is_synonymous(allele_info), is_blacklisted(chrom, pos, VARIANT_CLASS, hgmd_variants)
            
            # TODO: remove this line rare=True
            rare = True

            if splicing and rare and synonymous and not blacklist:
                OUT.write('\t'.join([chrom, pos, ID, ref, allele, qual,
                                     filt, ';'.join(info_fields + allele_info)]) + '\n')
                written += 1
            else:
                if not splicing or not synonymous:
                    splice_region += not splicing
                    nonsynonymous += not synonymous
                elif not rare:
                    common += not rare
            total += 1
    return total, written, common, nonsynonymous, splice_region
                

if __name__ == '__main__':
    import sys
    import gzip
    IN, OUT, VARIANT_CLASS = sys.argv[1:]
    if IN[-3:] == '.gz':
        IN = gzip.open(IN)
    elif IN == 'stdin':
        IN = sys.stdin
    else:
        IN = open(IN)
    if OUT[-3:] == '.gz':
        OUT = gzip.open(OUT, 'w')
    elif OUT == 'stdout':
        OUT = sys.stdout
    else:
        OUT = open(OUT, 'w')
    sys.stderr.write('\t'.join(['TOTAL', 'WRITTEN', 'COMMON', 'NONSYN', 'NON_SPLICING']) + '\n')
    sys.stderr.write('\t'.join(map(str, main(IN, OUT, VARIANT_CLASS))) + '\n')
