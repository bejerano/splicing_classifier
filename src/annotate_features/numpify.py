"""
Given an annotated VCF file
extract the specified features into a numpy array.

features to be extracted are specified in the FEATS dictionary
with the format NAME: (INFO_FIELD, index, type).

exon features are created in the function EXON_FEATS
(this distinction is made because additional post
processing is required.)

TODO:
   - Add more features by appending FEATS as they are added
   - Create a better way to handle a list of features (like
     multiple exons). I don't know what is a good policy
     at the moment, so we just always choose the last one.
   - Also, make sure we have a sound convention for the
     exon_positioning and that this logic is correct.
   - Overall, it seems that the exon features are the most
     fragile, so these could probably use a few double checks.
"""

import numpy as np
import sys
from collections import OrderedDict
import tabix

def parse(line):
    chrom, pos, _, ref, alt, _, _, info = line.split('\t')[:8]
    info = {el.split('=')[0]: el.split('=')[-1].split(',')[-1].split(':')
            for el in info.split(';')}
    return chrom, pos, ref, alt, info

def valid(info):
    return ('GENE_FEATURES' in info
            and 'TRANSCRIPT_FEATURES' in info)

#############################################################################

ID_FEATS = ['CHROM', 'POS', 'REF', 'ALT']
def identify(chrom, pos, ref, alt, info):
    return [chrom, pos, ref, alt]

##############################################################################

def dot_float(x):
    if x == '.':
        return float('inf')
    return float(x)

FEATS = OrderedDict({
    'RVIS': ('GENE_FEATURES', 1, dot_float),
    'pLI':  ('GENE_FEATURES', 2, dot_float),
    'HI':     ('GENE_FEATURES', 3, dot_float),
    'SPIDEX': ('SPIDEX', 1, dot_float),
    'PHYLOP100WAY': ('PHYLOP100WAY', 0, float),
    'PHASTCONS100WAY': ('PHASTCONS100WAY', 0, float),
    'PHYLOP_VERTEBRATES': ('PHYLOP_VERTEBRATES', 0, float),
    'PHYLOP_PLACENTAL':   ('PHYLOP_PLACENTAL', 0, float),
    'PHYLOP_PRIMATES':    ('PHYLOP_PRIMATES', 0, float),
    'PHCONS_VERTEBRATES': ('PHCONS_VERTEBRATES', 0, float),
    'PHCONS_PLACENTAL':   ('PHCONS_PLACENTAL', 0, float),
    'PHCONS_PRIMATES':    ('PHCONS_PRIMATES', 0, float),
    'LINSIGHT':           ('LINSIGHT', 0, dot_float),
    'EIGEN':              ('EIGEN', 1, dot_float),
    'EIGEN_NONCODING':    ('EIGEN_NONCODING', 2, dot_float),
    'CADD':               ('CADD_FE', 2, float),
    'CDTS':               ('CDTS', 0, dot_float),
    'TRAP':               ('TRAP', 1, dot_float),
    'ALFQMAX':            ('ALFQ', 2, float),
    'HMFQMAX':            ('HMFQ', 2, float),
    'ALCTMAX':            ('ALCT', 1, int),
    'HMCTMAX':            ('HMCT', 1, int),
    'gene_common_name':   ('SE', 3, str),
    'hg19':('EXON_FEATURES', 2, float),
     'panTro4':('EXON_FEATURES', 3, float),
     'gorGor3':('EXON_FEATURES', 4, float),
     'ponAbe2':('EXON_FEATURES', 5, float),
     'nomLeu3':('EXON_FEATURES', 6, float),
     'rheMac3':('EXON_FEATURES', 7, float),
     'macFas5':('EXON_FEATURES', 8, float),
     'papHam1':('EXON_FEATURES', 9, float),
     'chlSab1':('EXON_FEATURES', 10, float),
     'calJac3':('EXON_FEATURES', 11, float),
     'saiBol1':('EXON_FEATURES', 12, float),
     'otoGar3':('EXON_FEATURES', 13, float),
     'tupChi1':('EXON_FEATURES', 14, float),
     'speTri2':('EXON_FEATURES', 15, float),
     'jacJac1':('EXON_FEATURES', 16, float),
     'micOch1':('EXON_FEATURES', 17, float),
     'criGri1':('EXON_FEATURES', 18, float),
     'mesAur1':('EXON_FEATURES', 19, float),
     'mm10':('EXON_FEATURES', 20, float),
     'rn5':('EXON_FEATURES', 21, float),
     'hetGla2':('EXON_FEATURES', 22, float),
     'cavPor3':('EXON_FEATURES', 23, float),
     'chiLan1':('EXON_FEATURES', 24, float),
     'octDeg1':('EXON_FEATURES', 25, float),
     'oryCun2':('EXON_FEATURES', 26, float),
     'ochPri3':('EXON_FEATURES', 27, float),
'susScr3':('EXON_FEATURES', 28, float),
'vicPac2':('EXON_FEATURES', 29, float),
'camFer1':('EXON_FEATURES', 30, float),
'turTru2':('EXON_FEATURES', 31, float),
'orcOrc1':('EXON_FEATURES', 32, float),
'panHod1':('EXON_FEATURES', 33, float),
'bosTau7':('EXON_FEATURES', 34, float),
'oviAri3':('EXON_FEATURES', 35, float),
'capHir1':('EXON_FEATURES', 36, float),
'equCab2':('EXON_FEATURES', 37, float),
'cerSim1':('EXON_FEATURES', 38, float),
'felCat5':('EXON_FEATURES', 39, float),
'canFam3':('EXON_FEATURES', 40, float),
'musFur1':('EXON_FEATURES', 41, float),
'ailMel1':('EXON_FEATURES', 42, float),
'odoRosDiv1':('EXON_FEATURES', 43, float),
'lepWed1':('EXON_FEATURES', 44, float),
'pteAle1':('EXON_FEATURES', 45, float),
'pteVam1':('EXON_FEATURES', 46, float),
'myoDav1':('EXON_FEATURES', 47, float),
'myoLuc2':('EXON_FEATURES', 48, float),
'eptFus1':('EXON_FEATURES', 49, float),
'eriEur2':('EXON_FEATURES', 50, float),
'sorAra2':('EXON_FEATURES', 51, float),
'conCri1':('EXON_FEATURES', 52, float),
'loxAfr3':('EXON_FEATURES', 53, float),
'eleEdw1':('EXON_FEATURES', 54, float),
'triMan1':('EXON_FEATURES', 55, float),
'chrAsi1':('EXON_FEATURES', 56, float),
'echTel2':('EXON_FEATURES', 57, float),
'oryAfe1':('EXON_FEATURES', 58, float),
'dasNov3':('EXON_FEATURES', 59, float),
'monDom5':('EXON_FEATURES', 60, float),
'sarHar1':('EXON_FEATURES', 61, float),
'macEug2':('EXON_FEATURES', 62, float),
'ornAna1':('EXON_FEATURES', 63, float),
'falChe1':('EXON_FEATURES', 64, float),
'falPer1':('EXON_FEATURES', 65, float),
'ficAlb2':('EXON_FEATURES', 66, float),
'zonAlb1':('EXON_FEATURES', 67, float),
'geoFor1':('EXON_FEATURES', 68, float),
'taeGut2':('EXON_FEATURES', 69, float),
'pseHum1':('EXON_FEATURES', 70, float),
'melUnd1':('EXON_FEATURES', 71, float),
'amaVit1':('EXON_FEATURES', 72, float),
'araMac1':('EXON_FEATURES', 73, float),
'colLiv1':('EXON_FEATURES', 74, float),
'anaPla1':('EXON_FEATURES', 75, float),
'galGal4':('EXON_FEATURES', 76, float),
'melGal1':('EXON_FEATURES', 77, float),
'allMis1':('EXON_FEATURES', 78, float),
'cheMyd1':('EXON_FEATURES', 79, float),
'chrPic1':('EXON_FEATURES', 80, float),
'pelSin1':('EXON_FEATURES', 81, float),
'apaSpi1':('EXON_FEATURES', 82, float),
'anoCar2':('EXON_FEATURES', 83, float),
'xenTro7':('EXON_FEATURES', 84, float),
'latCha1':('EXON_FEATURES', 85, float),
'tetNig2':('EXON_FEATURES', 86, float),
'fr3':('EXON_FEATURES', 87, float),
'takFla1':('EXON_FEATURES', 88, float),
'oreNil2':('EXON_FEATURES', 89, float),
'neoBri1':('EXON_FEATURES', 90, float),
'hapBur1':('EXON_FEATURES', 91, float),
'mayZeb1':('EXON_FEATURES', 92, float),
'punNye1':('EXON_FEATURES', 93, float),
'oryLat2':('EXON_FEATURES', 94, float),
'xipMac1':('EXON_FEATURES', 95, float),
'gasAcu1':('EXON_FEATURES', 96, float),
'gadMor1':('EXON_FEATURES', 97, float),
'danRer7':('EXON_FEATURES', 98, float),
'astMex1':('EXON_FEATURES', 99, float),
'lepOcu1':('EXON_FEATURES', 100, float),
'petMar2':('EXON_FEATURES', 101, float),
'core3_rare':('EXON_FEATURES', 102, float),
'core3_common':('EXON_FEATURES', 103, float),
'extended3_rare':('EXON_FEATURES', 104, float),
'extended3_common':('EXON_FEATURES', 105, float),
'core5_rare':('EXON_FEATURES', 106, float),
'core5_common':('EXON_FEATURES', 107, float),
'extended5_rare':('EXON_FEATURES', 108, float),
'extended5_common':('EXON_FEATURES', 109, float),
'gnomad_alfq':('GNOMAD_ALFQ', 1, float),
'gnomad_hmfq':('GNOMAD_HMFQ', 1, float),
'BP_SCORE':('BP_SCORE', 1, dot_float),
'MPC': ('regional_constraint', 0, dot_float)
})

EXON_FEATS = ["5'Dist", "3'Dist", "Exon_length", "Exon_length%3", "Exon_pos", "total", "strand", "exon_name"]

def featurize_exon(pos, info):
    # Dist to 5' , Dist to 3', exon length, exon_pos, total
    strand = info['EXON_FEATURES'][1]
    exon_name =info['TRANSCRIPT_FEATURES'][0]
    chrom, start, end = info['TRANSCRIPT_FEATURES'][1].split('_')
    exon_pos, total = info['TRANSCRIPT_FEATURES'][2:4]

    exon_pos, total, pos= int(exon_pos)+1, int(total), int(pos)
    start, end = int(start), int(end)

    if strand == '+':
        five, three = pos - end - 1, pos - start
    else:
        five, three = start - pos, end - pos + 1
    return [five, three, end - start, (end - start) % 3, exon_pos, total, strand, exon_name]

def get_freq(chrom, pos, ref, alt, tb):
    max_freq = None
    results = tb.querys("%s:%s-%s"%(chrom, pos, pos))
    for record in results:
        chrom, p, identifier, ref, alts, qual, fil, info = record
        info_field, info_data = info.split("=")
        for alt_allele, freq_info in zip(alts.split(","), info.split(",")[1:]):
            if pos == p and alt==alt_allele:
                max_freq = max([0] + [float(f) for f in freq_info.split(":")[1:] if f != "."])
    return max_freq

def smart_get(feat, i, T, info, k, pos, freq, strand):
    if (feat=="GNOMAD_ALFQ" or feat=="GNOMAD_HMFQ") and feat not in info:
        return 0
    elif (feat=="GNOMAD_ALFQ" or feat=="GNOMAD_HMFQ"):
        return max([0] + [float(f.strip()) for f in info[feat][1:] if f.strip() != "."])

    if feat=="regional_constraint" and feat not in info:
        return float(info["GENE_FEATURES"][5]) if info["GENE_FEATURES"][5] != "." else float(1)

    if feat not in info:
        return float('inf')
    elif info.get(feat)[i] == ".":
        return float('inf')

    val = T(info[feat][i].strip())

    if freq is None:
        return val

    if k in ["extended5_rare", "extended5_common", 'core3_rare', 'core3_common', 'extended3_rare', 'extended3_common', 'core5_rare', 'core5_common']:
        exon_id = info[feat][0]
        chrom, exonStart, exonEnd = exon_id.split("_")
        exonStart, exonEnd, pos = int(exonStart), int(exonEnd), int(pos)
        strand = info[feat][1]
        if strand == '+':
            five, three = pos - exonEnd - 1, pos - exonStart
        else:
            five, three = exonStart - pos, exonEnd - pos + 1
        common = True if freq > .01 else False
        if -2 < three <= 0:
            # 3 prime core side
            if common and k == "core3_common":
                val = val - 1
            elif not common and k== "core3_rare":
                val = val - 1
        elif -6 < three <= -2:
            # 3 prime extended side
            if common and k == "extended3_common":
                val = val - 1
            elif not common and k == "extended3_rare":
                val = val - 1 
        elif 0 <= five < 2:
            # 5 prime core side
            if common and k == "core5_common":
                val = val - 1
            elif not common and k == "core5_rare":
                val = val - 1
        elif 2 <= five < 6:
            # 5 prime extended side
            if common and k == "extended5_common":
                val = val -1
            elif not common and k == "extended5_rare":
                val = val - 1
    return val


def featurize(pos, info, freq):
    exon_features = featurize_exon(pos, info) 
    return ([smart_get(feat, i, T, info, k, pos, freq, exon_features[6])
             for k, (feat, i, T) in FEATS.items()]
            + exon_features)

#######################################################################

def get_features(vcf, tb, out):
    out.write('\t'.join(ID_FEATS + FEATS.keys() + EXON_FEATS) + "\n")
    ID, features = [], []
    with open(vcf) as fp:
        for line in fp:
            if line[0] == '#': continue
            chrom, pos, ref, alt, info = parse(line)
            freq = get_freq(chrom, pos, ref, alt, tb)
            if not valid(info): continue
            #features += [featurize(pos, info, freq)]
            #ID += [identify(chrom, pos, ref, alt, info)]
            out.write("\t".join(np.array(identify(chrom, pos, ref, alt, info)).astype('str')) + "\t" + "\t".join(np.array(featurize(pos, info, freq)).astype('str')) + "\n")
#ID, features = np.array(ID), np.array(features)
#return (np.array(ID), ID_FEATS, np.array(features), FEATS.keys() + EXON_FEATS)

if __name__ == '__main__':
    vcf, tsv = sys.argv[1:]
    tb = tabix.open("/cluster/u/cdeisser/gnomad/formatted-gnomad-data/exome/ALFQ.vcf.gz")
    out = open(tsv, "w")
    #out.write('\t'.join(id_header + header) + "\n")
    get_features(vcf, tb, out)
    out.close()
    #np.savetxt(tsv, np.hstack([ID, features]).astype('str'), delimiter = '\t', header = '\t'.join(id_header + header), fmt='%s')
